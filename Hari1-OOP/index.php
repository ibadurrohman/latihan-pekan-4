<?php
    require 'animal.php';

    $sheep = new Animal("shaun");

    echo "Name: ";
    echo $sheep->name; // "shaun"
    echo "<br>Legs: ";
    echo $sheep->legs; // 4
    echo "<br>cold blooded: ";
    echo $sheep->cold_blooded; // "no"

    $sungokong = new Ape("kera sakti");
    echo "<br><br>Name: ";
    echo $sungokong->name; // "shaun"
    echo "<br>Legs: ";
    echo $sungokong->legs; // 4
    echo "<br>cold blooded: ";
    echo $sungokong->cold_blooded; // "no"
    echo "<br>Yell: ";
    $sungokong->yell(); // "Auooo"

    $kodok = new Frog("buduk");
    echo "<br><br>Name: ";
    echo $kodok->name; // "shaun"
    echo "<br>Legs: ";
    echo $kodok->legs; // 4
    echo "<br>cold blooded: ";
    echo $kodok->cold_blooded; // "no"
    echo "<br>Jump: ";
    $kodok->jump() ; // "hop hop"

?>