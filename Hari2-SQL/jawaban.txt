SOAL 1

CREATE DATABASE myshop

SOAL 2 

CREATE TABLE users(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    email varchar(255),
    pasword varchar(255),
    PRIMARY KEY(id)
);    

CREATE TABLE categories(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    PRIMARY KEY(id)
);

CREATE TABLE items(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    description varchar(255),
    price int,
    stock int,
    category_id int,
    PRIMARY KEY(id),
    FOREIGN KEY(category_id) REFERENCES categories(id)
);

SOAL 3

INSERT INTO users(name, email, pasword)
VALUES('JOHN DOE', 'john@doe.com','john123'),('JONE DOE', 'jane@doe.com', 'jenita123');

INSERT INTO categories(name)
VALUES('gadget'), ('cloth'), ('men'), ('women'), ('branded');

INSERT INTO items(name, description, price, stock, category_id)
VALUES ('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1),
	   ('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2),
       ('IMHO Watch','jam tangan anak yang jujur banget',2000000,10,1);